# Recipeitor

## Development Setup
* Virtual Environment
    * `pip install virtualenv`
    * `virtualenv VENV`
    * `source VENV/bin/activate`
* Install required packages
    * `pip install -r requirements.txt`
* Start server from the `recipeitor` directory
    * `python manage.py runserver`
