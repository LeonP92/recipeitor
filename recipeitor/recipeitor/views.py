from django.http import HttpResponse
from django.template import loader

def index(request):
    template = loader.get_template('recipes/index.html')
    return HttpResponse(template.render({'test_string': 'Hello friend!'}))
